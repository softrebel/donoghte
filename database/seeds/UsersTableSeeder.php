<?php

use Illuminate\Database\Seeder;
use App\User;
use \App\Models\Profile as Profile;
use \App\Models\Role as Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->name = 'محمدرضا شاقوزی';
        $admin->email = 'sh.mohammad66@yahoo.com';
        $admin->password = Hash::make('1234');
        $admin->save();
        $profile = new Profile;
        $profile->birth_date=date('y-m-d');
        $profile->blog_uri='shaghouzi';
        $profile->role()->associate(Role::where('name', 'admin')->first());
        $admin->profile()->save($profile);
//        $admin->profile()->add(Role::where('name', 'admin')->first());

        $admin = new User;
        $admin->name = 'سعید فتحی';
        $admin->email = 's.fathi@yahoo.com';
        $admin->password = Hash::make('1234');
        $admin->save();
        $profile = new Profile;
        $profile->birth_date=date('y-m-d');
        $profile->blog_uri='mentalist';
        $profile->role()->associate(Role::where('name', 'user')->first());
        $admin->profile()->save($profile);
    }
}
