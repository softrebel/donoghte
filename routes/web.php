<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');

Route::group(['prefix'=>'profile','as'=>'profile.'],function(){
    Route::get('/', 'ProfileController@index')->name('home');
//    Route::get('/posts', 'ProfileController@post');
    Route::post('/posts', 'ProfileController@savePost')->name('savePost');
});
Route::get('blog/{blog_uri}', ['uses' => 'BlogController@getPostsByBlog'])->name('blog_uri');
Route::get('lastActivity','BlogController@lastActivityOfBlogs')->name('lastActivity');

Route::get('blog/{blog_uri}/{post_id}', ['uses' => 'BlogController@getPostByID'])->name('blog_post');

Route::group(['prefix'=>'comment','as'=>'comment.'],function(){
    Route::post('/save', 'CommentController@saveComment')->name('saveComment');
});
