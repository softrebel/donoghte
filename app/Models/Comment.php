<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = 'comments';

    public function author()
    {
        return $this->belongsTo('App\User','author_id');
    }
    public function post()
    {
        return $this->belongsTo('App\Models\post');
    }
}
