<?php

namespace App\Models;

use App\User;
use App\Models\Comment as Comment;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = 'posts';

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }
    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }

    protected $guarded = ['id'];
}
