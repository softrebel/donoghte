<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = 'roles';

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}
