<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User as User;
class Profile extends Model
{
    //
    protected $table = 'profiles';

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
