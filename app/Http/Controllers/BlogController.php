<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\Profile as Profile;
use App\Models\Post as Post;
use Auth;
class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function getPostsByBlog($blog_uri)
    {

        $blog = Profile::where('blog_uri', $blog_uri)->first();

        if ($blog) {
            $posts = Post::with('comments')
                ->where('author_id', $blog->user_id)->take(10)->get();

            return view('blog.blogPosts', [
                'header' => 'مطالب '.$blog->blog_bio,
                'posts' => $posts
            ]);
        } else {
            abort(404);
        }
//        return view('post.create');
    }

    public function lastActivityOfBlogs()
    {
        $posts = Post::with('comments','comments.author','author','author.profile')->latest()->take(2)->get();

        $user = User::with('profile')->where('id',Auth::user()->id)->first();
        return view('blog.lastActivity', [
            'header' => 'آخرین مطالب وبلاگ ها',
            'posts' => $posts,
            'user'=>$user
        ]);
    }

    public function getPostByID($blog_uri,$post_id)
    {
        $post = Post::with('author','author.profile')->where('id', $post_id)->first();
        if($post->author->profile->blog_uri==$blog_uri){
            return view('blog.blogPost', [
                'header' => $post->title,
                'post' => $post
            ]);
        }
        else
            abort(404);
    }
}
