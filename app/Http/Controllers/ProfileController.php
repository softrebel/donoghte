<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Models\Post as Post;
use App\User;
use Auth;

class ProfileController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $user = User::with('profile')->where('id',Auth::user()->id)->first();


        return view('post.create', ['user' => $user]);
    }

    public function savePost()
    {
        $request = request();
        // with array & create method
//        $now=DateTime::dateTime();
//        $user_data=[
//            'title'=>$request->input('title'),
//            'description'=>$request->input('description'),
//            'content'=>$request->input('content'),
//            'author_id'=>auth()->user()->id,
//            'publish_date'=>new \DateTime(),
//            'is_enabled'=>1,
//            'comment_count'=>0,
//        ];
//        Post::create($user_data);

        $user = User::with('profile')->where('id',Auth::user()->id)->first();
        $post = new Post;
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->content = $request->input('content');
        $post->author()->associate(auth()->user()->id);
        $post->save();
        return view('post.create', [
            'message' => 'با موفقیت ثبت شد',
            'user' => $user
        ]);

    }
}
