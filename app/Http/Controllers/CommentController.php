<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment as Comment;
use App\User;
use Auth;
class CommentController extends Controller
{
    //
    public function saveComment()
    {
        $request = request();

        $user = User::with('profile')->where('id',Auth::user()->id)->first();

        $comment = new Comment;
        $comment->text = $request->input('text');
        $comment->author()->associate(auth()->user()->id);
        $comment->post()->associate( $request->input('post_id'));
        $comment->save();
        return view('post.create', [
            'message' => 'با موفقیت ثبت شد',
            'user' => $user
        ]);
        return redirect()->back()->with('success', 'Data saved successfully!');

    }
}
