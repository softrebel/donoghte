<!DOCTYPE html>

<html lang="fa">
<head>
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('css/customFont.css')}}" rel="stylesheet">
    @if (isset($header) && $header)
        <title>{{$header}}</title>
    @endif
</head>

<body data-page-name="home" class="body-home" data-login="false" data-app="production">


<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GS688K" height="0" width="0"
            class="page_speed_970244490"></iframe>
</noscript>


<section class="site-body">

    <div class="alert"></div>
    <div class="notify-msg"></div>

    <main id="app" class="site-main">
        <section class="page-inner">
            <section style='text-align:center;'>

                @if (isset($header) && $header)
                    <h1>{{$header}}</h1>
                @endif
            </section>

            <section class="page-content">
                <section class="page page-home page-">
                    <section class="poststream container">

                        <section class="poststream-inner">

                            <div class="poststream-content">


                                <div class="post-stream infinite-container">
                                    <div class="jscroll-inner">


                                        @if (isset($posts) && $posts)
                                            @foreach ($posts as $post)
                                                <article class="card card-post js-button-action">
                                                    <header class="post-meta" data-user-id="gmlu1apiyj8q">
                                                        <div class="postStream-author popover-box js-popover"
                                                             data-user-hash="gmlu1apiyj8q">
                                                            <div class="meta--avatar js-popover-append">
                                                                <a href="{{route('blog_uri', [$post->author->profile->blog_uri])}}"
                                                                   dideo-checked="true">
                                                                    <img src="{{asset('img/default-avatar.jpg')}}"
                                                                         alt="post">
                                                                </a>
                                                            </div>
                                                            <div class="meta--author">
                                                                <a class="link name "
                                                                   href="{{route('blog_uri', [$post->author->profile->blog_uri])}}"
                                                                   dideo-checked="true">{{$post->author->name}}</a>
                                                                <div class="meta--time">
                                                                    {{$post->publish_date}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="post-content">
                                                        <a href="{{route('blog_post', [$post->author->profile->blog_uri,$post->id])}}"
                                                           dideo-checked="true">
                                                            <h2 class="post--title">{{ $post->title }}</h2>
                                                            <div class="post--text">
                                                                {!! $post->content !!}
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <hr>
                                                    <footer class="">

                                                        @if (isset($post->comments) && $post->comments)
                                                            @foreach ($post->comments as $comment)
                                                                <div>
                                                                    <div class="post-action-buttons">
                                                                        <svg class="icon svg stroke" width="21px" height="21px" viewBox="25 7 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                            <path d="M35.6875,7 L32.3125,7 C28.27375,7 25,10.27375 25,14.3125 C25,16.92925 26.3854375,19.2079375 28.4520625,20.5 L28.375,20.5 L28.375,25 L33.4375,21.625 L35.6875,21.625 C39.72625,21.625 43,18.35125 43,14.3125 C43,10.27375 39.72625,7 35.6875,7 Z M35.7750243,19.5499022 L33.4083577,19.5499022 L30.4500244,21.3249022 L30.4500244,19.2238939 C28.3862911,18.4902272 26.9000244,16.5395022 26.9000244,14.2249023 C26.9000244,11.2837273 29.2838494,8.89990234 32.2250243,8.89990234 L35.7750243,8.89990234 C38.7161993,8.89990234 41.1000242,11.2837273 41.1000242,14.2249023 C41.1000242,17.1660772 38.7161993,19.5499022 35.7750243,19.5499022 Z"></path>
                                                                        </svg>
                                                                    </div>

                                                                    <p>{{ $comment->author->name }} گفت: </p>
                                                                    <p>{!! $post->content !!} </p>
                                                                </div>
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </footer>
                                                </article>

                                            @endforeach
                                        @else
                                            <article class="card card-post js-button-action">
                                                <div class="post-content">
                                                   <p>مطلبی جهت نمایش وجود ندارد</p>
                                                </div>
                                            </article>

                                        @endif


                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </main>

</section>
</body>

</html>
