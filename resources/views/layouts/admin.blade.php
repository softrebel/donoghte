<!DOCTYPE html>


<html lang="en" direction="rtl" style="direction: rtl;">

<!-- begin::Head -->
<head>

    <!--begin::Base Path (base relative path for assets of this page) -->
    <base href="../">

    <!--end::Base Path -->
    <meta charset="utf-8"/>
    <title>پروفایل کاربری</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{asset('css/customFont.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.bundle.rtl.css')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="body-pannel">
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="row">
        <div class="col-md-4">
            <!-- begin:: Aside Menu -->
            <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
                    <ul class="kt-menu__nav ">
                        <li class="kt-menu__section kt-menu__section--first">
                            <h4 class="kt-menu__section-text">{{$user->name}}</h4>
                            <i class="kt-menu__section-icon flaticon-more-v2"></i>
                        </li>

                        <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('profile.home')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-share"></i><span class="kt-menu__link-text">ایجاد مطلب</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('blog_uri',$user->profile->blog_uri)}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-suitcase"></i><span class="kt-menu__link-text">نمایش بلاگ شخصی</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('lastActivity')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-light"></i><span class="kt-menu__link-text">لیست 10 پست آخر وبسایت</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-light"></i><span class="kt-menu__link-text">خروج</span></a></li>
                         </ul>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>

            <!-- end:: Aside Menu -->
        </div>
        <div class="col-md-6">
            @yield('content')
        </div>
    </div>

</div>
</body>

<!-- end::Body -->
</html>
