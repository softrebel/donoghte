<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<h1>ثبت مطلب</h1>
        <div class="col-md-12">
            @if (isset($message) && $message)
                <div class="alert alert-success">{{$message}}</div>
            @endif
            <form action="{{ route('profile.savePost') }}" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="title">عنوان</label>
                    <input type="text" class="form-control" name="title" id="">
                </div>
                <div class="form-group">
                    <label for="description">خلاصه</label>
                    <input type="text" class="form-control" name="description" id="">
                </div>
                <div class="form-group">
                <label for="content">متن</label>
                <textarea name="content" class="form-control" id="editor1" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" name="submit_save_user" value="ذخیره اطلاعات">
                </div>
            </form>
        </div>
        <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'editor1');
            CKEDITOR.config.contentsLangDirection = 'rtl';
            CKEDITOR.config.extraPlugins = 'uploadfile';
        </script>
