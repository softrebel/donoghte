@extends('layouts.admin')
@section('content')
    <div class="flex-right position-ref full-height">
        <div class="container">
            <div class="row">
                @include('post.form')
            </div>
        </div>
    </div>
@endsection
